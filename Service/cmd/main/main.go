package main

import (
	"log"
	"urlshortener/internal/database"
	"urlshortener/internal/server/rest"
)

const (
	host = ""   // it will serve server on all interfaces
	port = 8080 // on specific port
)

func main() {
	database, err := database.New()
	if err != nil {
		log.Fatal(err)
	}
	server := rest.New(database, host, port)
	log.Fatal(server.Start())
}
