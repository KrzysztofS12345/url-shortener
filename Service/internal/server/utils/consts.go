package utils

// http protocol consts
const (
	HTTP  = "http"
	HTTPS = "https"
)

// http formats
const (
	HTTPFormat = "%s://%s/%s"
	HTTPFormatWithPort = "%s://%s:%d/%s"
)

// ports
const (
	DefaultHTTPPort = 80
)
