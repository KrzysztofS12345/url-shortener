package statistician

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// GetRedirectStatistics returns redirect statistics
func (ctrl *StatisticianController) GetRedirectStatistics(c *gin.Context) {
	c.JSON(http.StatusOK, ctrl.statistics.GetStatistics())
	return
}
