package statistician

import (
	"urlshortener/internal/server/statistics"

	"github.com/gin-gonic/gin"
)

// StatisticianController controls all operations related to statistics
type StatisticianController struct {
	statistics *statistics.Redirects
}

type redirectorController interface {
	GetRedirectStatistics(c *gin.Context)
}

// NewStatisticsController creates instance of a controller for statistics
func NewStatisticsController(statistics *statistics.Redirects) *StatisticianController {
	return &StatisticianController{statistics}
}
