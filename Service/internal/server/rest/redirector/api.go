package redirector

import (
	"errors"
	"net/http"
	"urlshortener/internal/database"
	"urlshortener/internal/server/utils"

	"github.com/gin-gonic/gin"
)

// Redirect redirects to matching short url
func (ctrl *RedirectorController) Redirect(c *gin.Context) {
	short, ok := c.Params.Get("short")
	if !ok {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "no matching param short"})
		return
	}

	ctrl.statistics.ReportRedirectTry()

	url, err := ctrl.urlPairDatabase.GetURL(short)
	if err != nil {
		var dbErr database.NotFoundError
		if errors.As(err, &dbErr) {
			c.AbortWithStatusJSON(dbErr.Status, gin.H{"error": dbErr.Reason})
			return
		}
		utils.SetInternalServerError(c, err)
		return
	}

	ctrl.statistics.ReportSuccessfulRedirect()
	c.Redirect(http.StatusFound, url)
}
