package redirector

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
	"urlshortener/internal/database"
	"urlshortener/internal/server/statistics"

	"github.com/stretchr/testify/assert"

	"github.com/gin-gonic/gin"
)

type errorDatabase struct{}

func (*errorDatabase) GetURL(short string) (string, error) {
	return "", fmt.Errorf("error")
}

type errorDatabaseButNotFound struct{}

func (*errorDatabaseButNotFound) GetURL(short string) (string, error) {
	return "", database.NotFoundError{Status: http.StatusNotFound, Reason: "error"}
}

type successDatabase struct{}

func (*successDatabase) GetURL(short string) (string, error) {
	return "http://google.com/", nil
}

func setupRouter(db urlPairDatabase) *gin.Engine {
	r := gin.Default()
	ctrl := NewRedirectorController(db, &statistics.Redirects{})
	r.GET("/:short", ctrl.Redirect)
	return r
}

func Test_Redirect(t *testing.T) {
	tests := []struct {
		name           string
		db             urlPairDatabase
		wantStatusCode int
		wantBody       string
	}{
		{
			name:           "Testcase1: database fail",
			db:             &errorDatabase{},
			wantStatusCode: http.StatusInternalServerError,
			wantBody:       `{"error":"error"}`,
		},
		{
			name:           "Testcase2: database not found short",
			db:             &errorDatabaseButNotFound{},
			wantStatusCode: http.StatusNotFound,
			wantBody:       `{"error":"error"}`,
		},
		{
			name:           "Testcase3: success",
			db:             &successDatabase{},
			wantStatusCode: http.StatusFound,
			wantBody:       "<a href=\"http://google.com/\">Found</a>.\n\n",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			router := setupRouter(tt.db)

			w := httptest.NewRecorder()
			req, _ := http.NewRequest("GET", "/short", nil)
			router.ServeHTTP(w, req)

			assert.Equal(t, tt.wantStatusCode, w.Result().StatusCode)
			assert.Equal(t, tt.wantBody, w.Body.String())
		})
	}
}
