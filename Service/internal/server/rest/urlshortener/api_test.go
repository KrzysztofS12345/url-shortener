package urlshortener

import (
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"urlshortener/internal/model"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

type errorDatabase struct{}

func (*errorDatabase) CreateURLPair(_ string) (string, error) {
	return "", fmt.Errorf("error")
}

func (*errorDatabase) ListURLs() ([]model.DBURLPair, error) {
	return nil, fmt.Errorf("error")
}

func (*errorDatabase) DeleteURL(_ string) error {
	return fmt.Errorf("error")
}

type successDatabase struct{}

func (*successDatabase) CreateURLPair(_ string) (string, error) {
	return "short", nil
}

func (*successDatabase) ListURLs() ([]model.DBURLPair, error) {
	return []model.DBURLPair{}, nil
}

func (*successDatabase) DeleteURL(_ string) error {
	return nil
}

func setupRouter(db urlPairDatabase) *gin.Engine {
	r := gin.Default()
	ctrl := NewURLShortenerController("localhost", 8080, db)
	r.POST("/api/", ctrl.CreateURLPair)
	r.GET("/api/", ctrl.ListURLs)
	r.DELETE("/api/:id", ctrl.DeleteURL)
	return r
}

func Test_URLShortenerController_CreateURLPair(t *testing.T) {
	tests := []struct {
		name           string
		db             urlPairDatabase
		inputBody      io.Reader
		wantStatusCode int
		wantBody       string
	}{
		{
			name:           "Testcase1: cannot bind json",
			inputBody:      strings.NewReader(``),
			wantStatusCode: http.StatusBadRequest,
			wantBody:       "{\"error\":\"EOF\"}",
		},
		{
			name:           "Testcase2: not an url",
			inputBody:      strings.NewReader(`{"url":"not an url"}`),
			wantStatusCode: http.StatusBadRequest,
			wantBody:       "{\"error\":\"parse \\\"not an url\\\": invalid URI for request\"}",
		},
		{
			name:           "Testcase3: create url error",
			db:             &errorDatabase{},
			inputBody:      strings.NewReader(`{"url":"http://test.com/"}`),
			wantStatusCode: http.StatusInternalServerError,
			wantBody:       "{\"error\":\"error\"}",
		},
		{
			name:           "Testcase4: create url success",
			db:             &successDatabase{},
			inputBody:      strings.NewReader(`{"url":"http://test.com/"}`),
			wantStatusCode: http.StatusCreated,
			wantBody:       "{\"short\":\"http://localhost:8080/short\"}",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			router := setupRouter(tt.db)

			w := httptest.NewRecorder()
			req, _ := http.NewRequest("POST", "/api/", tt.inputBody)
			router.ServeHTTP(w, req)

			assert.Equal(t, tt.wantStatusCode, w.Result().StatusCode)
			assert.Equal(t, tt.wantBody, w.Body.String())
		})
	}
}

func Test_URLShortenerController_ListURLs(t *testing.T) {
	tests := []struct {
		name           string
		db             urlPairDatabase
		wantStatusCode int
		wantBody       string
	}{
		{
			name:           "Testcase1: list urls error",
			db:             &errorDatabase{},
			wantStatusCode: http.StatusInternalServerError,
			wantBody:       "{\"error\":\"error\"}",
		},
		{
			name:           "Testcase2: successful listing",
			db:             &successDatabase{},
			wantStatusCode: http.StatusOK,
			wantBody:       "[]",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			router := setupRouter(tt.db)

			w := httptest.NewRecorder()
			req, _ := http.NewRequest("GET", "/api/", nil)
			router.ServeHTTP(w, req)

			assert.Equal(t, tt.wantStatusCode, w.Result().StatusCode)
			assert.Equal(t, tt.wantBody, w.Body.String())
		})
	}
}

func Test_URLShortenerController_DeleteURL(t *testing.T) {
	tests := []struct {
		name           string
		db             urlPairDatabase
		wantStatusCode int
		wantBody       string
	}{
		{
			name:           "Testcase1: delete url error",
			db:             &errorDatabase{},
			wantStatusCode: http.StatusInternalServerError,
			wantBody:       "{\"error\":\"error\"}",
		},
		{
			name:           "Testcase2: success",
			db:             &successDatabase{},
			wantStatusCode: http.StatusNoContent,
			wantBody:       "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			router := setupRouter(tt.db)

			w := httptest.NewRecorder()
			req, _ := http.NewRequest("DELETE", "/api/id", nil)
			router.ServeHTTP(w, req)

			assert.Equal(t, tt.wantStatusCode, w.Result().StatusCode)
			assert.Equal(t, tt.wantBody, w.Body.String())
		})
	}
}
