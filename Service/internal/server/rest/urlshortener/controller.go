package urlshortener

import (
	"fmt"
	"urlshortener/internal/model"
	"urlshortener/internal/server/utils"
)

// URLShortenerController controls all operations related to shortening urls
type URLShortenerController struct {
	host string
	port int

	urlPairDatabase urlPairDatabase
}

type urlPairDatabase interface {
	CreateURLPair(url string) (string, error)
	ListURLs() ([]model.DBURLPair, error)
	DeleteURL(id string) error
}

// NewController creates instance of a controller for shortening a url
func NewURLShortenerController(host string, port int, urlPairDB urlPairDatabase) *URLShortenerController {
	if host == "" {
		host = "localhost"
	}
	return &URLShortenerController{host, port, urlPairDB}
}

func (ctrl *URLShortenerController) getShortURL(short string) string {
	if ctrl.port == utils.DefaultHTTPPort {
		return fmt.Sprintf(utils.HTTPFormat, utils.HTTP, ctrl.host, short)
	}
	return fmt.Sprintf(utils.HTTPFormatWithPort, utils.HTTP, ctrl.host, ctrl.port, short)
}
