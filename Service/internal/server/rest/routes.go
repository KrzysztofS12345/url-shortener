package rest

import (
	"urlshortener/internal/database"
	"urlshortener/internal/server/rest/redirector"
	"urlshortener/internal/server/rest/statistician"
	"urlshortener/internal/server/rest/urlshortener"
	"urlshortener/internal/server/statistics"

	"github.com/gin-gonic/gin"
)

const (
	apiPrefix           = "/api"
	apiStatisticsPrefix = apiPrefix + "/statistics/"
)

var statisticsControllerFactory = func(statistics *statistics.Redirects) statisticsController {
	return statistician.NewStatisticsController(statistics)
}

var urlShortenerControllerFactory = func(host string, port int, database *database.Database) urlShortenerController {
	return urlshortener.NewURLShortenerController(host, port, database)
}

var redirectorControllerFactory = func(database *database.Database, statistics *statistics.Redirects) redirectorController {
	return redirector.NewRedirectorController(database, statistics)
}

type statisticsController interface {
	GetRedirectStatistics(c *gin.Context)
}

type urlShortenerController interface {
	CreateURLPair(c *gin.Context)
	ListURLs(c *gin.Context)
	DeleteURL(c *gin.Context)
}

type redirectorController interface {
	Redirect(c *gin.Context)
}

func (s *Server) setupStatisticsRoutes(router *gin.Engine, statisticsController statisticsController) {
	statisticsAPI := router.Group(apiStatisticsPrefix)
	{
		statisticsAPI.GET("redirects", statisticsController.GetRedirectStatistics)
	}
}

func (s *Server) setupURLShortenerRoutes(router *gin.Engine, shortURLController urlShortenerController) {
	urlShortenerAPI := router.Group(apiPrefix)
	{
		urlShortenerAPI.POST("", shortURLController.CreateURLPair)
		urlShortenerAPI.GET("", shortURLController.ListURLs)
		urlShortenerAPI.DELETE(":id", shortURLController.DeleteURL)
	}
}

func (s *Server) setupRedirectRoutes(router *gin.Engine, redirectorController redirectorController) {
	router.GET(":short", redirectorController.Redirect)
}

func (s *Server) setupSwaggerRoute(router *gin.Engine) {
	router.Static("/api/swagger", "/resources/swagger-ui-dist")
}
