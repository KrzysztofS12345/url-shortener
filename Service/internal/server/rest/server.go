package rest

import (
	"fmt"
	"urlshortener/internal/database"
	"urlshortener/internal/server/statistics"

	"github.com/gin-gonic/gin"
)

type Server struct {
	database *database.Database
	stats    *statistics.Redirects
	host     string
	port     int
}

// New creates new instance of a server at specified url and port
func New(database *database.Database, host string, port int) *Server {
	redirectStatistics := statistics.NewRedirectStatistics()
	return &Server{database, redirectStatistics, host, port}
}

// Start setups the server and start to listen
func (s *Server) Start() error {
	router := gin.New()
	router.Use(gin.Recovery())
	s.setupStatisticsRoutes(router, statisticsControllerFactory(s.stats))
	s.setupURLShortenerRoutes(router, urlShortenerControllerFactory(s.host, s.port, s.database))
	s.setupRedirectRoutes(router, redirectorControllerFactory(s.database, s.stats))
	s.setupSwaggerRoute(router)
	return router.Run(fmt.Sprintf("%s:%d", s.host, s.port))
}
