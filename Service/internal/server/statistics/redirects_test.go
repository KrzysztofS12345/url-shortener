package statistics

import (
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_ReportRedirectTry(t *testing.T) {
	stats := NewRedirectStatistics()

	var wg sync.WaitGroup
	for i := 0; i < 100; i++ {
		wg.Add(1)
		go func(wg *sync.WaitGroup) {
			stats.ReportRedirectTry()
			wg.Done()
		}(&wg)
	}
	wg.Wait()

	assert.Equal(t, stats.GetStatistics().RedirectTryCount, 100)
}

func Test_ReportSuccessfulRedirect(t *testing.T) {
	stats := NewRedirectStatistics()

	var wg sync.WaitGroup
	for i := 0; i < 100; i++ {
		wg.Add(1)
		go func(wg *sync.WaitGroup) {
			stats.ReportSuccessfulRedirect()
			wg.Done()
		}(&wg)
	}
	wg.Wait()

	assert.Equal(t, stats.GetStatistics().SuccessfulRedirectCount, 100)
}
