package log

import "log"

// PrintErr prints err with 'ERR: <error message>' format
func PrintErr(err error) {
	log.Printf("ERR: %v\n", err)
}
