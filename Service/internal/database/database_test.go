package database

import (
	"testing"

	"github.com/hashicorp/go-memdb"
	"github.com/stretchr/testify/assert"
)

func TestNew(t *testing.T) {
	tests := []struct {
		name             string
		swapSchemaGetter func()
		assertion        assert.ErrorAssertionFunc
	}{
		{
			name: "Testcase1: error creating database",
			swapSchemaGetter: func() {
				getSchema = func() *memdb.DBSchema { return nil }
			},
			assertion: assert.Error,
		},
		{
			name:             "Testcase2: successful database creation",
			swapSchemaGetter: func() {},
			assertion:        assert.NoError,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			backupGetSchema := getSchema
			defer func() {
				getSchema = backupGetSchema
			}()

			tt.swapSchemaGetter()
			_, err := New()

			tt.assertion(t, err)
		})
	}
}
