package database

import (
	"strings"

	"github.com/hashicorp/go-memdb"
)

const (
	urlTable   = "url"
	idIndex    = "id"
	longIndex  = "long"
	shortIndex = "short"
)

var getSchema = func() *memdb.DBSchema {
	return schema
}

var schema = &memdb.DBSchema{
	Tables: map[string]*memdb.TableSchema{
		urlTable: &urlPairSchema,
	},
}

var urlPairSchema = memdb.TableSchema{
	Name: urlTable,
	Indexes: map[string]*memdb.IndexSchema{
		idIndex:    &idSchema,
		longIndex:  &longSchema,
		shortIndex: &shortSchema,
	},
}

var idSchema = memdb.IndexSchema{
	Name:    idIndex,
	Unique:  true,
	Indexer: &memdb.StringFieldIndex{Field: strings.ToUpper(idIndex)},
}

var longSchema = memdb.IndexSchema{
	Name:    longIndex,
	Unique:  true,
	Indexer: &memdb.StringFieldIndex{Field: strings.Title(longIndex)},
}

var shortSchema = memdb.IndexSchema{
	Name:    shortIndex,
	Unique:  true,
	Indexer: &memdb.StringFieldIndex{Field: strings.Title(shortIndex)},
}
