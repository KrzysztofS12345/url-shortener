package database

import (
	"fmt"
	"testing"
	"urlshortener/internal/model"

	"github.com/hashicorp/go-memdb"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

var testURLPairs = []model.DBURLPair{
	{ID: "1", Long: "https://long.com/", Short: "l"},
	{ID: "2", Long: "https://evenlonger.com/", Short: "ll"},
}

func writeInitialDataToDatabase(database *Database) error {
	txn := database.client.Txn(true)
	defer txn.Commit()

	for _, msg := range testURLPairs {
		if err := txn.Insert(urlTable, msg); err != nil {
			return err
		}
	}
	return nil
}

func TestDatabase_CreateURLPair(t *testing.T) {
	type args struct {
		url string
	}
	tests := []struct {
		name      string
		mock      func()
		args      args
		want      string
		assertion assert.ErrorAssertionFunc
	}{
		{
			name: "Testcase1: check if url exist error",
			mock: func() {
				firstMatchingItemByURLGetter = func(_ *memdb.Txn, _ string) (interface{}, error) {
					return "", fmt.Errorf("error")
				}
			},
			want:      "",
			assertion: assert.Error,
		},
		{
			name: "Testcase2: url already exist",
			mock: func() {
				firstMatchingItemByURLGetter = func(_ *memdb.Txn, _ string) (interface{}, error) {
					return model.DBURLPair{Short: "short"}, nil
				}
			},
			want:      "short",
			assertion: assert.NoError,
		},
		{
			name: "Testcase3: new url pair error",
			mock: func() {
				firstMatchingItemByURLGetter = func(_ *memdb.Txn, _ string) (interface{}, error) {
					return nil, nil
				}
				dbURLPairFactory = func(_ *memdb.Txn, _ string) (*model.DBURLPair, error) {
					return nil, fmt.Errorf("error")
				}
			},
			want:      "",
			assertion: assert.Error,
		},
		{
			name: "Testcase4: insert error",
			mock: func() {
				firstMatchingItemByURLGetter = func(_ *memdb.Txn, _ string) (interface{}, error) {
					return nil, nil
				}
				dbURLPairFactory = func(_ *memdb.Txn, url string) (*model.DBURLPair, error) {
					return &model.DBURLPair{ID: "id", Long: url, Short: "short"}, nil
				}
			},
			args: args{
				url: "",
			},
			want:      "",
			assertion: assert.Error,
		},
		{
			name: "Testcase5: insert success",
			mock: func() {
				firstMatchingItemByURLGetter = func(_ *memdb.Txn, _ string) (interface{}, error) {
					return nil, nil
				}
				dbURLPairFactory = func(_ *memdb.Txn, url string) (*model.DBURLPair, error) {
					return &model.DBURLPair{ID: "id", Long: url, Short: "short"}, nil
				}
			},
			args: args{
				url: "http://test.com/",
			},
			want:      "short",
			assertion: assert.NoError,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			backupFirstMatchingItemByURLGetter := firstMatchingItemByURLGetter
			backupURLPairFactory := dbURLPairFactory
			defer func() {
				firstMatchingItemByURLGetter = backupFirstMatchingItemByURLGetter
				dbURLPairFactory = backupURLPairFactory
			}()
			tt.mock()
			db, err := New()
			require.Nil(t, err)

			got, err := db.CreateURLPair(tt.args.url)

			tt.assertion(t, err)
			assert.Equal(t, tt.want, got)
		})
	}
}

func TestDatabase_ListURLs(t *testing.T) {
	tests := []struct {
		name      string
		mock      func(*Database) error
		want      []model.DBURLPair
		assertion assert.ErrorAssertionFunc
	}{
		{
			name: "Testcase1: email iterator error",
			mock: func(_ *Database) error {
				idIteratorGetter = func(_ *memdb.Txn) (memdb.ResultIterator, error) {
					return nil, fmt.Errorf("error")
				}
				return nil
			},
			want:      nil,
			assertion: assert.Error,
		},
		{
			name:      "Testcase2: empty database",
			mock:      func(database *Database) error { return nil },
			want:      []model.DBURLPair{},
			assertion: assert.NoError,
		},
		{
			name: "Testcase3: some items in db",
			mock: func(database *Database) error {
				return writeInitialDataToDatabase(database)
			},
			want:      testURLPairs,
			assertion: assert.NoError,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			backupIDIteratorGetter := idIteratorGetter
			defer func() {
				idIteratorGetter = backupIDIteratorGetter
			}()

			db, err := New()
			require.Nil(t, err)
			err = tt.mock(db)
			require.Nil(t, err)

			got, err := db.ListURLs()
			tt.assertion(t, err)
			assert.Equal(t, tt.want, got)
		})
	}
}

func TestDatabase_DeleteURL(t *testing.T) {
	type args struct {
		id string
	}
	tests := []struct {
		name      string
		mock      func(*Database) error
		args      args
		assertion assert.ErrorAssertionFunc
	}{
		{
			name: "Testcase1: first maching item getter error",
			mock: func(_ *Database) error {
				firstMatchingItemByIDGetter = func(txn *memdb.Txn, _ string) (interface{}, error) {
					return nil, fmt.Errorf("error")
				}
				return nil
			},
			assertion: assert.Error,
		},
		{
			name: "Testcase2: obj not found (nil)",
			mock: func(database *Database) error {
				return writeInitialDataToDatabase(database)
			},
			args:      args{id: "not in db id"},
			assertion: assert.NoError,
		},
		{
			name: "Testcase3: delete error",
			mock: func(database *Database) error {
				firstMatchingItemByIDGetter = func(txn *memdb.Txn, _ string) (interface{}, error) {
					return model.DBURLPair{}, nil
				}
				objectDeletor = func(txn *memdb.Txn, obj interface{}) error {
					return fmt.Errorf("error")
				}
				return nil
			},
			assertion: assert.Error,
		},
		{
			name: "Testcase4: successful delete",
			mock: func(database *Database) error {
				return writeInitialDataToDatabase(database)
			},
			args:      args{id: "1"},
			assertion: assert.NoError,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			backupFirstMatchingItemByIDGetter := firstMatchingItemByIDGetter
			backupObjectDeletor := objectDeletor
			defer func() {
				firstMatchingItemByIDGetter = backupFirstMatchingItemByIDGetter
				objectDeletor = backupObjectDeletor
			}()

			db, err := New()
			require.Nil(t, err)
			err = tt.mock(db)
			require.Nil(t, err)

			tt.assertion(t, db.DeleteURL(tt.args.id))
		})
	}
}

func TestDatabase_GetURL(t *testing.T) {
	type args struct {
		short string
	}
	tests := []struct {
		name      string
		mock      func()
		args      args
		want      string
		assertion assert.ErrorAssertionFunc
	}{
		{
			name: "Testcase1: first maching item getter error",
			mock: func() {
				firstMatchingItemByShortGetter = func(txn *memdb.Txn, _ string) (interface{}, error) {
					return nil, fmt.Errorf("error")
				}
			},
			assertion: assert.Error,
		},
		{
			name: "Testcase2: not found",
			mock: func() {
				firstMatchingItemByShortGetter = func(txn *memdb.Txn, _ string) (interface{}, error) {
					return nil, nil
				}
			},
			assertion: assert.Error,
		},
		{
			name: "Testcase3: success",
			args: args{"short"},
			mock: func() {
				firstMatchingItemByShortGetter = func(txn *memdb.Txn, short string) (interface{}, error) {
					return model.DBURLPair{ID: "id", Long: "http://test.com/", Short: short}, nil
				}
			},
			want:      "http://test.com/",
			assertion: assert.NoError,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			backupFirstMatchingItemByShortGetter := firstMatchingItemByShortGetter
			defer func() {
				firstMatchingItemByShortGetter = backupFirstMatchingItemByShortGetter
			}()

			db, err := New()
			require.Nil(t, err)
			tt.mock()

			actualURL, err := db.GetURL(tt.args.short)
			assert.Equal(t, tt.want, actualURL)
			tt.assertion(t, err)
		})
	}
}

func Test_newUniqueURLPair(t *testing.T) {
	type args struct {
		url string
	}
	tests := []struct {
		name      string
		mock      func()
		args      args
		want1     string
		want2     int
		assertion assert.ErrorAssertionFunc
	}{
		{
			name: "Testcase1: error",
			args: args{"http://test.com/"},
			mock: func() {
				firstMatchingItemByShortGetter = func(_ *memdb.Txn, _ string) (interface{}, error) {
					return nil, fmt.Errorf("error")
				}
			},
			assertion: assert.Error,
		},
		{
			name: "Testcase2: first generation success",
			args: args{"http://test.com/"},
			mock: func() {
				firstMatchingItemByShortGetter = func(_ *memdb.Txn, _ string) (interface{}, error) {
					return nil, nil
				}
			},
			want1:     "http://test.com/",
			want2:     2,
			assertion: assert.NoError,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			backupFirstMatchingItemByShortGetter := firstMatchingItemByShortGetter
			defer func() {
				firstMatchingItemByShortGetter = backupFirstMatchingItemByShortGetter
			}()

			tt.mock()

			actualURLPair, err := newUniqueURLPair(nil, tt.args.url)
			if err == nil {
				assert.Equal(t, tt.want1, actualURLPair.Long)
				assert.Equal(t, tt.want2, len(actualURLPair.Short))
			}
			tt.assertion(t, err)
		})
	}
}
