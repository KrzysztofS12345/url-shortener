package database

import (
	"fmt"
	"urlshortener/internal/log"
	"urlshortener/internal/model"

	"github.com/hashicorp/go-memdb"
)

const (
	shortMinimalLength = 2
	shortMaximumLength = 1000
)

var dbURLPairFactory = func(txn *memdb.Txn, url string) (*model.DBURLPair, error) {
	return newUniqueURLPair(txn, url)
}

var idIteratorGetter = func(txn *memdb.Txn) (memdb.ResultIterator, error) {
	return getIDIterator(txn)
}

var firstMatchingItemByIDGetter = func(txn *memdb.Txn, id string) (interface{}, error) {
	return getFirstMatchingItemByID(txn, id)
}

var firstMatchingItemByURLGetter = func(txn *memdb.Txn, url string) (interface{}, error) {
	return getFirstMatchingItemByURL(txn, url)
}

var firstMatchingItemByShortGetter = func(txn *memdb.Txn, short string) (interface{}, error) {
	return getFirstMatchingItemByShort(txn, short)
}

var objectDeletor = func(txn *memdb.Txn, obj interface{}) error {
	return deleteObject(txn, obj)
}

// CreateURLPair checks if given url already exists and returns its short if true
// otherwise it tries to create a new record in the database and then return new short
func (db *Database) CreateURLPair(url string) (string, error) {
	txn := db.client.Txn(true)
	defer txn.Commit()

	obj, err := firstMatchingItemByURLGetter(txn, url)
	if err != nil {
		log.PrintErr(err)
		return "", err
	}

	if obj != nil {
		dbURLPair := obj.(model.DBURLPair)
		return dbURLPair.Short, nil
	}

	databaseURLPair, err := dbURLPairFactory(txn, url)
	if err != nil {
		log.PrintErr(err)
		return "", err
	}

	if err := txn.Insert(urlTable, *databaseURLPair); err != nil {
		log.PrintErr(err)
		return "", err
	}

	return databaseURLPair.Short, nil
}

// ListURLs returns all url pairs with their ids
func (db *Database) ListURLs() ([]model.DBURLPair, error) {
	urlPairs := []model.DBURLPair{}
	txn := db.client.Txn(false)
	defer txn.Abort()

	iterator, err := idIteratorGetter(txn)
	if err != nil {
		log.PrintErr(err)
		return nil, err
	}

	for obj := iterator.Next(); obj != nil; obj = iterator.Next() {
		urlPairs = append(urlPairs, obj.(model.DBURLPair))
	}
	return urlPairs, nil
}

// DeleteURL deletes a url pair by given id
func (db *Database) DeleteURL(id string) error {
	txn := db.client.Txn(true)
	defer txn.Commit()

	obj, err := firstMatchingItemByIDGetter(txn, id)
	if err != nil {
		log.PrintErr(err)
		return err
	}

	if obj == nil {
		return nil
	}

	if err := objectDeletor(txn, obj); err != nil {
		log.PrintErr(err)
		return err
	}

	return nil
}

// GetURL returns url by short
// Can return NotFoundError type
func (db *Database) GetURL(short string) (string, error) {
	txn := db.client.Txn(true)
	defer txn.Commit()

	obj, err := firstMatchingItemByShortGetter(txn, short)
	if err != nil {
		log.PrintErr(err)
		return "", err
	}

	if obj == nil {
		return "", newNotFoundError(fmt.Sprintf("Short %s was not found in the database", short))
	}

	return obj.(model.DBURLPair).Long, nil
}


// It's not completely safe as there is a slight chance that it won't generate unique short within it's iterations
//  and then database will return error due to not unique short.
// Also there is no detection whether all posibilities are made 
// but it's unlikely that it will happened as it has numberOfAvailableCharacter^shortMaximumLength possibilities
func newUniqueURLPair(txn *memdb.Txn, url string) (*model.DBURLPair, error) {
	databaseURLPair := model.NewDBURLPair(url)

	for i := 0; i < shortMaximumLength; i++ {
		databaseURLPair.RollShort(i + shortMinimalLength)
		obj, err := firstMatchingItemByShortGetter(txn, databaseURLPair.Short)
		if err != nil {
			log.PrintErr(err)
			return nil, err
		}

		if obj == nil {
			break
		}
	}

	return databaseURLPair, nil
}

func getIDIterator(txn *memdb.Txn) (memdb.ResultIterator, error) {
	return txn.Get(urlTable, idIndex)
}

func getFirstMatchingItemByID(txn *memdb.Txn, id string) (interface{}, error) {
	return txn.First(urlTable, idIndex, id)
}

func getFirstMatchingItemByURL(txn *memdb.Txn, url string) (interface{}, error) {
	return txn.First(urlTable, longIndex, url)
}

func getFirstMatchingItemByShort(txn *memdb.Txn, short string) (interface{}, error) {
	return txn.First(urlTable, shortIndex, short)
}

func deleteObject(txn *memdb.Txn, obj interface{}) error {
	return txn.Delete(urlTable, obj)
}
