package model

import (
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func Test_NewDBURLPair(t *testing.T) {
	longURL := "https://longurl.com/"
	short := "sh"

	backupShortFactory := shortFactory
	defer func() {
		shortFactory = backupShortFactory
	}()

	shortFactory = func(_ int) string { return short }

	urlPair := NewDBURLPair(longURL)
	urlPair.RollShort(0)

	assert.Equal(t, urlPair.Long, longURL)
	assert.Equal(t, urlPair.Short, short)
	_, idErr := uuid.Parse(urlPair.ID)
	assert.Nil(t, idErr)
}
