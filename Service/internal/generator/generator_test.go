package generator

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_RandLetters(t *testing.T) {
	emptyString := RandLetters(0)
	assert.Empty(t, emptyString)

	oneLetterString := RandLetters(1)
	assert.True(t, len(oneLetterString) == 1)
}
