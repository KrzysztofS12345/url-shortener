module urlshortener

go 1.16

require (
	github.com/gin-gonic/gin v1.7.2
	github.com/google/uuid v1.2.0
	github.com/hashicorp/go-memdb v1.3.2
	github.com/stretchr/testify v1.7.0
)
