# requires vscode extension: https://marketplace.visualstudio.com/items?itemName=humao.rest-client

### Create a new short
POST http://localhost:8080/api

{"url": "https://google.com/"}

### Get all shorts
GET http://localhost:8080/api

### Delete specific short
DELETE http://localhost:8080/api/6769daac-b88b-400b-b104-df8baf91d804

### Get statistics
GET http://localhost:8080/api/statistics/redirects

### Redirect
GET http://localhost:8080/short
