# URL Shortener Application

The application provides url shortening and redirect functionalities.


## Preconditions
- install Docker (for windows or linux)
- install terminal allowing to run bash scripts (for example git bash)

## How to

### Run:

1. Download the repository
2. Go to the project
3. If using linux, make scripts executable `chmod +x ./scripts/*`
4. Run `./scripts/run.sh` (necessarily this way)

The service should be available at <http://localhost:8080/>

### Stop:

1. Go to the project
2. Run `./scripts/stop.sh` (necessarily this way)

### Use:

The technical API documentation is available via swagger at <http://localhost:8080/api/swagger>

#### curl examples

##### Create new short url

```
curl -XPOST -H "Content-type: application/json" -d '{"url":"https://google.com/"}' 'http://localhost:8080/api'
```

##### List all created shorts

```
curl -XGET -H "Content-type: application/json" 'http://localhost:8080/api'
```

##### Delete a short

```
curl -XDELETE 'http://localhost:8080/api/1fecf3ba-968c-464d-b3ba-f8b498c891d0'
```

##### Redirect by short

```
curl -XGET 'http://localhost:8080/short'
```

##### Get statistics

```
curl -XGET -H "Content-type: application/json" 'http://localhost:8080/api/statistics/redirects'
```